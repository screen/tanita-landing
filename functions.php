<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'tanita_jquery_enqueue');
function tanita_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script('jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.1.js', array('jquery'), '3.3.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action('after_setup_theme', 'tanita_register_navwalker');
function tanita_register_navwalker()
{
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if (class_exists('WooCommerce')) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if (defined('JETPACK__VERSION')) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus(array(
    'header_menu' => __('Menu Header - Principal', 'tanita')
));

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action('widgets_init', 'tanita_widgets_init');

function tanita_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Principal', 'tanita'),
        'id' => 'main_sidebar',
        'description' => __('Estos widgets seran vistos en las entradas y páginas del sitio', 'tanita'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));

    register_sidebars(4, array(
        'name'          => __('Pie de Página %d', 'tanita'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'tanita'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));

    //    register_sidebar( array(
    //        'name' => __( 'Sidebar de la Tienda', 'tanita' ),
    //        'id' => 'shop_sidebar',
    //        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'tanita' ),
    //        'before_widget' => '<li id='%1$s' class='widget %2$s'>',
    //        'after_widget'  => '</li>',
    //        'before_title'  => '<h2 class='widgettitle'>',
    //        'after_title'   => '</h2>',
    //    ) );
}



/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(9999, 400, true);
}
if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true);
    add_image_size('product_img', 400, 600, array('center', 'center'));
}

/* --------------------------------------------------------------
    SENDINBLUE INTEGRATION
-------------------------------------------------------------- */
function sendinblue_add_contact($email, $nombre, $apellido, $phone, $state)
{
    $sendinblue_settings = get_option('tnt_sendinblue_settings');
    $createContact['email'] = $email;
    $createContact['attributes'] = array('NOMBRE' => $nombre, 'APELLIDOS' => $apellido, 'SMS' => '+52' . $phone, 'ESTADO' => $state);
    $createContact['listIds'] = array(intval($sendinblue_settings['list_id']));
    $createContact['emailBlacklisted'] = false;
    $createContact['smsBlacklisted'] = false;
    $createContact['updateEnabled'] = false;

    $cliente = curl_init();
    curl_setopt($cliente, CURLOPT_URL, "https://api.sendinblue.com/v3/contacts");
    curl_setopt($cliente, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'api-key:' . $sendinblue_settings['apikey']));
    curl_setopt($cliente, CURLOPT_POST, 1);
    curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
    $respuesta = json_decode(curl_exec($cliente));
    $http_code = curl_getinfo($cliente, CURLINFO_HTTP_CODE);
    curl_close($cliente);
    if ($http_code == 400) {
        $createContact['email'] = $email;
        $createContact['attributes'] = array('NOMBRE' => $nombre, 'APELLIDOS' => $apellido, 'SMS' =>  '+52' . $phone, 'ESTADO' => $state);
        $createContact['listIds'] = array(intval($sendinblue_settings['list_id']));
        $createContact['emailBlacklisted'] = false;
        $createContact['smsBlacklisted'] = false;
        $createContact['updateEnabled'] = true;
        curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
        $cliente = curl_init();
        curl_setopt($cliente, CURLOPT_URL, "https://api.sendinblue.com/v3/contacts");
        curl_setopt($cliente, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'api-key:' . $sendinblue_settings['apikey']));
        curl_setopt($cliente, CURLOPT_POST, 1);
        curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
        $respuesta = json_decode(curl_exec($cliente));
        $http_code = curl_getinfo($cliente, CURLINFO_HTTP_CODE);
        curl_close($cliente);
    }

    $content = true;
    return $content;
    wp_die();
}

/* --------------------------------------------------------------
    AJAX HANDLER INTEGRATION
-------------------------------------------------------------- */

add_action('wp_ajax_subscribe_contact', 'subscribe_contact_handler');
add_action('wp_ajax_nopriv_subscribe_contact', 'subscribe_contact_handler');

function subscribe_contact_handler()
{
    $email = $_POST['form_email'];

    if (isset($_POST['form_name'])) {
        $name = explode(' ', $_POST['form_name']);
        $nombre = $name[0];
        $apellido = $name[1];
    } else {
        $nombre = '';
        $apellido = '';
    }

    if (isset($_POST['form_phone'])) {
        $phone = $_POST['form_phone'];
    } else {
        $phone = '';
    }

    if (isset($_POST['form_estado'])) {
        $state = $_POST['form_estado'];
    } else {
        $state = '';
    }
    $content = sendinblue_add_contact($email, $nombre, $apellido, $phone, $state);

    ob_start();
    $logo = get_template_directory_uri() . '/images/logo.png';
    require_once get_theme_file_path('/templates/contact-email.php');
    $body = ob_get_clean();
    $body = str_replace([
        '{name}',
        '{lastname}',
        '{email}',
        '{phone}',
        '{state}',
        '{logo}'
    ], [
        $nombre,
        $apellido,
        $email,
        $phone,
        $state,
        $logo
    ], $body);

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML(true);
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress('rochoa@screen.com.ve');
    $mail->setFrom("noreply@{$_SERVER['SERVER_NAME']}", esc_html(get_bloginfo('name')));
    $mail->Subject = esc_html__('Tanita: Nuevo Suscriptor', 'tanita');

    if (!$mail->send()) {
        wp_send_json_success(esc_html__("Thank You for your interest in our products, you will be contacted shortly.", 'tanita'), 200);
    } else {
        wp_send_json_success(esc_html__("Thank You for your interest in our products, you will be contacted shortly.", 'tanita'), 200);
    }
}
