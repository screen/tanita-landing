/* CUSTOM ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');
    AOS.init();

    const Countdown = (() => {

        let nextMidnight = new Date();
        nextMidnight.setHours(24, 0, 0, 0);

        const getRemainingTime = () => {
            let now = new Date();

            let time = (nextMidnight.getTime() - now.getTime()) / 1000;

            if (time < 0) {
                nextMidnight = new Date();
                nextMidnight.setHours(24, 0, 0, 0);

                return getRemainingTime();
            }

            return time;
        }

        const parseTime = (time) => {
            const hours = Math.floor(time / 3600);
            let rest = time - (hours * 3600);
            const minutes = Math.floor(rest / 60);
            rest = rest - (minutes * 60);
            const seconds = Math.floor(rest);
            const milliseconds = (rest - seconds) * 1000;

            return [hours, minutes, seconds, milliseconds];
        };

        const formatTime = (parsedTime) => {
            return '<div class="hours"><span>' + parsedTime[0] + '</span><small>Horas</small></div><div class="minutes"><span>' + ("0" + parsedTime[1]).slice(-2) + '</span><small>Minutos</small></div><div class="seconds"><span>' + ("0" + parsedTime[2]).slice(-2) + '</span><small>Segundos</small></div>';
        };

        const els = [];
        let timeout;

        return (el) => {
            els.push(el);

            if (!timeout) {

                const refresh = () => {
                    const parsedTime = parseTime(getRemainingTime());
                    const formattedTimes = formatTime(parsedTime);

                    for (let i = 0, iend = els.length; i < iend; i++) {
                        els[i].innerHTML = formattedTimes;
                    }

                    setTimeout(() => {
                        refresh();
                    }, parsedTime[3]);
                };
                refresh();

            } else el.innerHTML = formatTime(parseTime(getRemainingTime()));
        };

    })();

    //Countdown(document.getElementById('countdown'));
    Countdown(document.getElementById('countdown-two'));
}

var mySwiper = new Swiper('.test-swiper-container', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,
    pagination: {
        el: '.swiper-pagination',
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        991: {
            slidesPerView: 2,
        },
        1024: {
            slidesPerView: 3
        }
    }
});

var mySwiper2 = new Swiper('.tips-swiper-container', {
    slidesPerView: 4,
    spaceBetween: 35,
    centeredSlides: true,
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 2,
        },
        991: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4
        }
    }
});

document.addEventListener("DOMContentLoaded", documentCustomLoad, false);