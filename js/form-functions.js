var formSubscribe = document.getElementById('subscribeForm'),
    formSubscribeBtn = document.getElementById('subscribeFormBtn'),
    validForm = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validatePhone(str) {
    var patt = new RegExp(/^[0-9]{11}$/);
    return patt.test(str);
}

/* CUSTOM ON LOAD FUNCTIONS */
function formDocumentCustomLoad() {
    "use strict";
    console.log('Form Functions Correctly Loaded');
    if (formSubscribe) {
        formSubscribeBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementById('formEstado');
            var errorText = document.getElementById('errorEstado');
            if (elements.value == '') {
                errorText.classList.remove('d-none');
                errorText.innerHTML = custom_admin_url.error_estado;
                validForm = false;
            } else {
                errorText.classList.add('d-none');
                errorText.innerHTML = '';
                validForm = true;
            }

            var elements = document.getElementById('formName');
            var errorText = document.getElementById('errorName');
            if (elements.value == '') {
                errorText.classList.remove('d-none');
                errorText.innerHTML = custom_admin_url.error_nombre;
                validForm = false;
            } else {
                if (elements.value == '') {
                    errorText.classList.remove('d-none');
                    errorText.innerHTML = custom_admin_url.invalid_nombre;
                    validForm = false;
                } else {
                    errorText.classList.add('d-none');
                    errorText.innerHTML = '';
                    validForm = true;
                }
            }

            var elements = document.getElementById('formPhone');
            var errorText = document.getElementById('errorPhone');
            if (elements.value == '') {
                errorText.classList.remove('d-none');
                errorText.innerHTML = custom_admin_url.error_phone;
                validForm = false;
            } else {
                if (validatePhone(elements.value) == false) {
                    errorText.classList.remove('d-none');
                    errorText.innerHTML = custom_admin_url.invalid_phone;
                    validForm = false;
                } else {
                    var str = elements.value;
                    var three = str.substring(0, 3);
                    if ((three != '+52') && (three != '052')) {
                        errorText.classList.remove('d-none');
                        errorText.innerHTML = custom_admin_url.country_phone;
                        validForm = false;
                    } else {
                        errorText.classList.add('d-none');
                        errorText.innerHTML = '';
                        validForm = true;
                    }
                }
            }

            var elements = document.getElementById('formEmail');
            var errorText = document.getElementById('errorEmail');
            if (elements.value == '') {
                errorText.classList.remove('d-none');
                errorText.innerHTML = custom_admin_url.error_email;
                validForm = false;
            } else {
                if (validateEmail(elements.value) == false) {
                    errorText.classList.remove('d-none');
                    errorText.innerHTML = custom_admin_url.invalid_email;
                    validForm = false;
                } else {
                    errorText.classList.add('d-none');
                    errorText.innerHTML = '';
                    validForm = true;
                }
            }



            if (validForm == true) {
                submitForm();
            }
        });
    }
}

document.addEventListener("DOMContentLoaded", formDocumentCustomLoad, false);

function submitForm() {
    var emailForm = document.getElementsByClassName('input-form-control');
    var info = 'action=subscribe_contact&form_name=' + emailForm[0].value + '&form_phone=' + emailForm[1].value + '&form_email=' + emailForm[2].value + '&form_estado=' + emailForm[3].value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        window.location = custom_admin_url.thanks_url;
    };
    newRequest.send(info);
}