<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'tanita_customize_register');

function tanita_customize_register($wp_customize)
{

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('tnt_social_settings', array(
        'title'    => __('Redes Sociales', 'tanita'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'tanita'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('tnt_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'tanita_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'tnt_social_settings',
        'settings' => 'tnt_social_settings[facebook]',
        'label' => __('Facebook', 'tanita'),
    ));

    $wp_customize->add_setting('tnt_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'tanita_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'tnt_social_settings',
        'settings' => 'tnt_social_settings[twitter]',
        'label' => __('Twitter', 'tanita'),
    ));

    $wp_customize->add_setting('tnt_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'tanita_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'tnt_social_settings',
        'settings' => 'tnt_social_settings[instagram]',
        'label' => __('Instagram', 'tanita'),
    ));

    $wp_customize->add_setting('tnt_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'tanita_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'tnt_social_settings',
        'settings' => 'tnt_social_settings[linkedin]',
        'label' => __('LinkedIn', 'tanita'),
    ));

    $wp_customize->add_setting('tnt_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'tanita_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'tnt_social_settings',
        'settings' => 'tnt_social_settings[youtube]',
        'label' => __('YouTube', 'tanita'),
    ));

    /* COOKIES SETTINGS */
    $wp_customize->add_section('tnt_cookie_settings', array(
        'title'    => __('Cookies', 'tanita'),
        'description' => __('Opciones de Cookies', 'tanita'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('tnt_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'tanita'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'tnt_cookie_settings',
        'settings' => 'tnt_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('tnt_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'tnt_cookie_settings',
        'settings' => 'tnt_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'tanita'),
    ));

    /* SENDINBLUE SETTINGS */
    $wp_customize->add_section('tnt_sendinblue_settings', array(
        'title'    => __('Sendinblue', 'tanita'),
        'description' => __('Opciones para Sendinblue', 'tanita'),
        'priority' => 180
    ));

    $wp_customize->add_setting('tnt_sendinblue_settings[apikey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('apikey', array(
        'type' => 'text',
        'label'    => __('APIKey', 'tanita'),
        'description' => __('Agregar APIkey de Sendinblue', 'tanita'),
        'section'  => 'tnt_sendinblue_settings',
        'settings' => 'tnt_sendinblue_settings[apikey]'
    ));

    $wp_customize->add_setting('tnt_sendinblue_settings[list_id]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('list_id', array(
        'type'     => 'number',
        'section' => 'tnt_sendinblue_settings',
        'settings' => 'tnt_sendinblue_settings[list_id]',
        'label' => __('ID de Lista en Sendinblue', 'tanita'),
    ));

    $wp_customize->add_setting('tnt_sendinblue_settings[thanks_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('thanks_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'tnt_sendinblue_settings',
        'settings' => 'tnt_sendinblue_settings[thanks_link]',
        'label' => __('Link de Pagina de Agradecimiento', 'tanita'),
    ));
}

function tanita_sanitize_url($url)
{
    return esc_url_raw($url);
}