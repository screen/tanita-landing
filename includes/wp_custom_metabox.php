<?php
function ed_metabox_include_front_page($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'])) {
        return $display;
    }

    if ('front-page' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter('cmb2_show_on', 'ed_metabox_include_front_page', 10, 2);

function be_metabox_show_on_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'be_metabox_show_on_slug', 10, 2);

add_action('cmb2_admin_init', 'tanita_register_custom_metabox');
function tanita_register_custom_metabox()
{
    $prefix = 'tnt_';

    require_once('custom-metaboxes-landing.php');

    /* --------------------------------------------------------------
    1.- PRELAUNCH OPTIONS
-------------------------------------------------------------- */
    $cmb_thanks_metabox = new_cmb2_box(array(
        'id'            => $prefix . 'thanks_metabox',
        'title'         => esc_html__('Pre Launch: Información Extra', 'tanita'),
        'object_types'  => array('page'),
        'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-thanks.php'),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ));

    $cmb_thanks_metabox->add_field(array(
        'id'   => $prefix . 'thanks_button_subtitle',
        'name'      => esc_html__('Subtítulo anterior al boton de acción', 'tanita'),
        'desc'      => esc_html__('Ingresa un sobretitulo que estara arriba del boton de acción', 'tanita'),
        'type' => 'text'
    ));

    $cmb_thanks_metabox->add_field(array(
        'id'   => $prefix . 'thanks_button_text',
        'name'      => esc_html__('Texto del boton de acción', 'tanita'),
        'desc'      => esc_html__('Ingresa un texto descriptivo para el boton de acción', 'tanita'),
        'type' => 'text'
    ));

    $cmb_thanks_metabox->add_field(array(
        'id'   => $prefix . 'thanks_button_link',
        'name'      => esc_html__('Link URL del boton de acción', 'tanita'),
        'desc'      => esc_html__('Ingresa un link descriptivo para el boton de acción', 'tanita'),
        'type' => 'text_url'
    ));

   

    $cmb_thanks_products = new_cmb2_box(array(
        'id'            => $prefix . 'products_metabox',
        'title'         => esc_html__('Pre Launch: Productos Extra', 'tanita'),
        'object_types'  => array('page'),
        'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-thanks.php'),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ));

    $cmb_thanks_products->add_field(array(
        'id'   => $prefix . 'main_contact_title',
        'name'      => esc_html__('Título antes de los productos', 'tanita'),
        'desc'      => esc_html__('Ingresa un sobretitulo que estara arriba de los productos', 'tanita'),
        'type' => 'text'
    ));

    $cmb_thanks_products->add_field(array(
        'id'   => $prefix . 'main_contact_subtitle',
        'name'      => esc_html__('Título despues del contador', 'tanita'),
        'desc'      => esc_html__('Ingresa un sobretitulo que estara debajo del contador', 'tanita'),
        'type' => 'text'
    ));

    $group_field_id = $cmb_thanks_products->add_field(array(
        'id'            => $prefix . 'main_contact_group',
        'name'          => esc_html__('Grupos de Items', 'tanita'),
        'description'   => __('Items dentro del Itemr', 'tanita'),
        'type'          => 'group',
        'options'       => array(
            'group_title'       => __('Item {#}', 'tanita'),
            'add_button'        => __('Agregar otro Item', 'tanita'),
            'remove_button'     => __('Remover Item', 'tanita'),
            'sortable'          => true,
            'closed'            => true,
            'remove_confirm'    => esc_html__('¿Estas seguro de remover este Item?', 'tanita')
        )
    ));
    
    $cmb_thanks_products->add_group_field($group_field_id, array(
        'id'        => 'title',
        'name'      => esc_html__('Título del Item', 'tanita'),
        'desc'      => esc_html__('Ingrese el Título del Item', 'tanita'),
        'type'      => 'text'
    ));

    $cmb_thanks_products->add_group_field($group_field_id, array(
        'id'        => 'link',
        'name'      => esc_html__('Link URL del Item', 'tanita'),
        'desc'      => esc_html__('Ingrese el Link URL del Item', 'tanita'),
        'type'      => 'text_url'
    ));

    $cmb_thanks_products->add_group_field($group_field_id, array(
        'id'        => 'image',
        'name'      => esc_html__( 'Imagen del Producto', 'tanita' ),
        'desc'      => esc_html__( 'Cargue una imagen para este producto', 'tanita' ),
        'type'    => 'file',
        
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Image', 'tanita' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );
    
    
}
