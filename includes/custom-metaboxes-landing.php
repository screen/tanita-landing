<?php
/* --------------------------------------------------------------
    1.- MAIN HERO
-------------------------------------------------------------- */
$cmb_landing_hero = new_cmb2_box( array(
    'id'            => $prefix . 'landing_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_hero->add_field( array(
    'id'         => $prefix . 'hero_main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'tanita' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'tanita' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_landing_hero->add_field( array(
    'id'         => $prefix . 'hero_form_intro',
    'name'      => esc_html__( 'Intro Principal del formulario', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el intro principal para el formulario', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_hero->add_field( array(
    'id'         => $prefix . 'form_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'tanita' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
    2.- HERO 2
-------------------------------------------------------------- */
$cmb_landing_hero2 = new_cmb2_box( array(
    'id'            => $prefix . 'landing_hero2_metabox',
    'title'         => esc_html__( '2.- Hero Secundario', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_hero2->add_field( array(
    'id'         => $prefix . 'hero2_main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'tanita' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'tanita' ),
    'type'    => 'file',
    
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_landing_hero2->add_field( array(
    'id'         => $prefix . 'hero2_first_intro',
    'name'      => esc_html__( 'Primer Cuadro de texto', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto principal para el cuadro', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_hero2->add_field( array(
    'id'         => $prefix . 'hero2_second_intro',
    'name'      => esc_html__( 'Segundo Cuadro de texto', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto secundario para el cuadro', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_hero2->add_field( array(
    'id'         => $prefix . 'hero2_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_hero2->add_field( array(
    'id'         => $prefix . 'hero2_button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'tanita' ),
    'type'      => 'text_url'
) );

/* --------------------------------------------------------------
    3.- TIPS
-------------------------------------------------------------- */
$cmb_landing_tips = new_cmb2_box( array(
    'id'            => $prefix . 'landing_tips_metabox',
    'title'         => esc_html__( '3.- Tips', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_tips->add_field( array(
    'id'         => $prefix . 'tips_main_title',
    'name'      => esc_html__( 'Texto del Título Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el Título de la sección', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_tips->add_field( array(
    'id'            => $prefix . 'tips_gallery_list',
    'name'          => esc_html__('Galería', 'tanita'),
    'desc'          => esc_html__('Cargue o seleccione las imágenes para incluirlas en el Slider', 'tanita'),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar Imágenes', 'tanita' ),
        'remove_image_text'         => esc_html__( 'Remove Imágenes', 'tanita' ),
        'file_text'                 => esc_html__( 'Imagen', 'tanita' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tanita' ),
        'remove_text'               => esc_html__( 'Remover', 'tanita' ),
    ),
    'preview_size'  => 'thumbnail'
) );

$cmb_landing_tips->add_field( array(
    'id'         => $prefix . 'tips_content',
    'name'      => esc_html__( 'Contenido luego del Slider', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto secundario para el espacio luego del slider', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$group_field_id = $cmb_landing_tips->add_field( array(
    'id'          => $prefix . 'landing_tips_group',
    'name'      => esc_html__( 'Grupos de Tips', 'tanita' ),
    'description' => __( 'Tips dentro de la Sección', 'tanita' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'tanita' ),
        'add_button'        => __( 'Agregar otro Item', 'tanita' ),
        'remove_button'     => __( 'Remover Item', 'tanita' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Item?', 'tanita' )
    )
) );

$cmb_landing_tips->add_group_field( $group_field_id, array(
    'id'   => 'image',
    'name'      => esc_html__( 'Imagen del Item', 'tanita' ),
    'desc'      => esc_html__( 'Cargar una Imagen para este Item', 'tanita' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_landing_tips->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Título del Item', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el titulo del item', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_tips->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Item', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva del Item', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_tips->add_field( array(
    'id'         => $prefix . 'tips_post_content',
    'name'      => esc_html__( 'Contenido luego del grupo de tips', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto secundario para el espacio luego del grupo', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_tips->add_field( array(
    'id'         => $prefix . 'tips_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_tips->add_field( array(
    'id'         => $prefix . 'tips_button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'tanita' ),
    'type'      => 'text_url'
) );

/* --------------------------------------------------------------
    4.- TESTIMONIALS SECTION
-------------------------------------------------------------- */

$cmb_landing_test = new_cmb2_box( array(
    'id'            => $prefix . 'landing_test_metabox',
    'title'         => esc_html__( '4.- Sección Testimoniales', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_test->add_field( array(
    'id'   => $prefix . 'landing_test_title',
    'name'      => esc_html__( 'Título de la Sección', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tanita' ),
    'type' => 'text'
) );

$group_field_id = $cmb_landing_test->add_field( array(
    'id'          => $prefix . 'landing_test_group',
    'name'      => esc_html__( 'Grupos de Testimonios', 'tanita' ),
    'description' => __( 'Testimonios dentro de la Sección', 'tanita' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'tanita' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'tanita' ),
        'remove_button'     => __( 'Remover Testimonio', 'tanita' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Testimonio?', 'tanita' )
    )
) );

$cmb_landing_test->add_group_field( $group_field_id, array(
    'id'   => 'image',
    'name'      => esc_html__( 'Imagen del Testimonio', 'tanita' ),
    'desc'      => esc_html__( 'Cargar una Imagen para este Testimonio', 'tanita' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_landing_test->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Autor del Testimonio', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el nombre del autor del testimonio', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_test->add_group_field( $group_field_id, array(
    'id'        => 'location',
    'name'      => esc_html__( 'Ubicación del Autor', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese la ubicación del autor del testimonio', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_test->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Testimonio', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva del Testimonio', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    5.- DESCANSO 1
-------------------------------------------------------------- */
$cmb_landing_descanso1 = new_cmb2_box( array(
    'id'            => $prefix . 'landing_descanso1_metabox',
    'title'         => esc_html__( '5.- Descanso # 1', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_descanso1->add_field( array(
    'id'         => $prefix . 'descanso1_main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Descanso', 'tanita' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este descanso', 'tanita' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_landing_descanso1->add_field( array(
    'id'         => $prefix . 'descanso1_content',
    'name'      => esc_html__( 'Primer Cuadro de texto', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto principal para el cuadro', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_descanso1->add_field( array(
    'id'         => $prefix . 'descanso1_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_descanso1->add_field( array(
    'id'         => $prefix . 'descanso1_button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'tanita' ),
    'type'      => 'text_url'
) );

/* --------------------------------------------------------------
    6.- DESCANSO 2
-------------------------------------------------------------- */
$cmb_landing_descanso2 = new_cmb2_box( array(
    'id'            => $prefix . 'landing_descanso2_metabox',
    'title'         => esc_html__( '5.- Descanso # 2', 'tanita' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_landing_descanso2->add_field( array(
    'id'         => $prefix . 'descanso2_main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Descanso', 'tanita' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este descanso', 'tanita' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tanita' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_landing_descanso2->add_field( array(
    'id'         => $prefix . 'descanso2_content',
    'name'      => esc_html__( 'Primer Cuadro de texto', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto principal para el cuadro', 'tanita' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_landing_descanso2->add_field( array(
    'id'         => $prefix . 'descanso2_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'tanita' ),
    'type' => 'text'
) );

$cmb_landing_descanso2->add_field( array(
    'id'         => $prefix . 'descanso2_button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'tanita' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'tanita' ),
    'type'      => 'text_url'
) );

