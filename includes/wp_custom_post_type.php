<?php
/*
function tanita_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'tanita' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'tanita' ),
		'menu_name'             => __( 'Clientes', 'tanita' ),
		'name_admin_bar'        => __( 'Clientes', 'tanita' ),
		'archives'              => __( 'Archivo de Clientes', 'tanita' ),
		'attributes'            => __( 'Atributos de Cliente', 'tanita' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'tanita' ),
		'all_items'             => __( 'Todos los Clientes', 'tanita' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'tanita' ),
		'add_new'               => __( 'Agregar Nuevo', 'tanita' ),
		'new_item'              => __( 'Nuevo Cliente', 'tanita' ),
		'edit_item'             => __( 'Editar Cliente', 'tanita' ),
		'update_item'           => __( 'Actualizar Cliente', 'tanita' ),
		'view_item'             => __( 'Ver Cliente', 'tanita' ),
		'view_items'            => __( 'Ver Clientes', 'tanita' ),
		'search_items'          => __( 'Buscar Cliente', 'tanita' ),
		'not_found'             => __( 'No hay resultados', 'tanita' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'tanita' ),
		'featured_image'        => __( 'Imagen del Cliente', 'tanita' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'tanita' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'tanita' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'tanita' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'tanita' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'tanita' ),
		'items_list'            => __( 'Listado de Clientes', 'tanita' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'tanita' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'tanita' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'tanita' ),
		'description'           => __( 'Portafolio de Clientes', 'tanita' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'tanita_custom_post_type', 0 );
*/
