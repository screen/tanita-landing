<?php

/**
 * Template Name: Pagina Landing
 * @package tanita
 * @subpackage tanita-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $main_bg = get_post_meta(get_the_ID(), 'tnt_hero_main_bg', true); ?>
        <section id="subscribe" class="hero-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $main_bg; ?>);">
            <div class="container container-hero-special">
                <div class="row">
                    <article class="hero-main-content col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="300">
                        <?php the_content(); ?>
                    </article>
                    <aside class="hero-form-content col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_hero_form_intro', true)); ?>
                        <?php echo get_template_part('templates/element-subscribe-form'); ?>
                    </aside>
                </div>
            </div>
        </section>

        <?php $main_bg = get_post_meta(get_the_ID(), 'tnt_hero2_main_bg', true); ?>
        <section class="hero-boxes-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $main_bg; ?>);">
            <div class="container container-hero-special">
                <div class="row align-items-center justify-content-end">
                    <div class="hero-box hero-first-box col-xl-5 offset-xl-1 offset-lg-1 col-lg-5 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_hero2_first_intro', true)); ?>
                    </div>
                    <div class="hero-box hero-second-box col-xl-5 col-lg-5 offset-xl-1 offset-lg-1 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="700">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_hero2_second_intro', true)); ?>
                        <div data-aos="fade" data-aos-delay="1000">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'tnt_hero2_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'tnt_hero2_button_text', true); ?>" class="btn btn-md btn-boxes"><?php echo get_post_meta(get_the_ID(), 'tnt_hero2_button_text', true); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="tips-main-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="tips-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tnt_tips_main_title', true); ?></h2>
                    </div>
                </div>
            </div>
            <div class="container container-hero-special">
                <div class="row">
                    <div class="tips-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $arr_tips = get_post_meta(get_the_ID(), 'tnt_tips_gallery_list', true); ?>
                        <?php if (!empty($arr_tips)) { ?>
                        <div class="tips-swiper-container swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($arr_tips as $key => $value) { ?>
                                <div class="swiper-slide">
                                    <?php $bg_banner_id = $key; ?>
                                    <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                    <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="tips-description col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_tips_content', true)); ?>
                    </div>
                    <?php $arr_steps = get_post_meta(get_the_ID(), 'tnt_landing_tips_group', true); ?>
                    <?php $i = 1; ?>
                    <?php if (!empty($arr_steps)) { ?>
                    <?php foreach ($arr_steps as $item) { ?>
                        <?php $delay = 200 * $i; ?>
                    <article class="tips-step-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                        <div class="step-item-image">
                            <div class="picture">
                                <?php $bg_banner_id = $item['image_id']; ?>
                                <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </div>
                        </div>
                        <div class="step-item-body">
                            <h3><?php echo $item['author']; ?></h3>
                            <?php echo apply_filters( 'the_content', $item['desc'] ); ?>
                        </div>
                    </article>
                    <?php $i++; } ?>
                    <?php } ?>
                    <div class="tips-cta-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_tips_post_content', true)); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tnt_tips_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'tnt_tips_button_text', true); ?>" class="btn btn-md btn-boxes"><?php echo get_post_meta(get_the_ID(), 'tnt_tips_button_text', true); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="test-main-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="test-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tnt_landing_test_title', true); ?></h2>
                    </div>
                    <div class="test-main-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $arr_test = get_post_meta(get_the_ID(), 'tnt_landing_test_group', true); ?>
                        <?php if (!empty($arr_test)) { ?>
                        <div class="test-swiper-container swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($arr_test as $item) { ?>
                                <div class="swiper-slide">
                                    <div class="test-item-container">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/quote.png" alt="quote" class="img-fluid" />
                                        <div class="test-item-content">
                                            <?php echo apply_filters('the_content', $item['desc']); ?>
                                        </div>
                                        <div class="media">
                                            <?php $bg_banner_id = $item['image_id']; ?>
                                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'avatar', false); ?>
                                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-circle" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                            <div class="media-body">
                                                <div class="star-container">
                                                    <?php $n = rand(4, 5); ?>
                                                    <?php for ($i = 1; $i <= $n; $i++) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <?php } ?>
                                                    <?php if ($n < 5) { ?>
                                                    <i class="fa fa-star-o"></i>
                                                    <?php } ?>
                                                </div>
                                                <h4><?php echo $item['author']; ?></h4>
                                                <p><?php echo $item['location']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="descanso-section descanso1 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <article class="descanso-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12" data-aos="fade" data-aos-delay="600">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_descanso1_content', true)); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tnt_descanso1_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'tnt_descanso1_button_text', true); ?>" class="btn btn-md btn-boxes"><?php echo get_post_meta(get_the_ID(), 'tnt_descanso1_button_text', true); ?></a>
                    </article>
                    <picture class="descanso-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1" data-aos="fade" data-aos-delay="400">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tnt_descanso1_main_bg_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </picture>
                </div>
            </div>
        </section>

        <section class="descanso-section descanso2 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <picture class="descanso-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="400">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tnt_descanso2_main_bg_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </picture>
                    <article class="descanso-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="600">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tnt_descanso2_content', true)); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tnt_descanso2_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'tnt_descanso2_button_text', true); ?>" class="btn btn-md btn-boxes"><?php echo get_post_meta(get_the_ID(), 'tnt_descanso2_button_text', true); ?></a>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>