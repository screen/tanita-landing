<?php

/**
 * Template Name: Pagina Gracias
 * @package tanita
 * @subpackage tanita-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="thanks-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-between">
                    <article class="thanks-main-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <svg class="thanks-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#00AAAA" stroke-width="7" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                            <polyline class="path check" fill="none" stroke="#00AAAA" stroke-width="7" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
                        </svg>
                        <?php the_content(); ?>
                        <?php $button_link = get_post_meta(get_the_ID(), 'tnt_thanks_button_link', true); ?>
                        <?php if ($button_link != '') { ?>
                            <div class="button-thanks-container">
                                <?php $button_text = get_post_meta(get_the_ID(), 'tnt_thanks_button_text', true); ?>
                                <h5><?php echo get_post_meta(get_the_ID(), 'tnt_thanks_button_subtitle', true); ?></h5>
                                <a href="<?php echo $button_link; ?>" title="<?php echo $button_text; ?>" class="btn btn-md btn-thanks-action"><?php echo $button_text; ?></a>
                            </div>
                        <?php } ?>
                    </article>
                    <div class="products-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tnt_main_contact_title', true); ?></h2>
                        <div class="countdown-container" id="countdown-two"></div>
                        <h2><?php echo get_post_meta(get_the_ID(), 'tnt_main_contact_subtitle', true); ?></h2>
                    </div>
                    <?php $arr_group = get_post_meta(get_the_ID(), 'tnt_main_contact_group', true); ?>
                    <?php foreach ($arr_group as $item) { ?>
                        <article class="product-item col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                            <picture>
                                <?php $bg_banner_id = $item['image_id']; ?>
                                <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'product_img', false); ?>
                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </picture>
                            <header>
                                <h3><?php echo $item['title']; ?></h3>
                            </header>
                            <a href="<?php echo $item['link']; ?>"><?php _e('Ver producto en descuento', 'tanita'); ?></a>
                        </article>
                    <?php } ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>