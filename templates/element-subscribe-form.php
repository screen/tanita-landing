<form id="subscribeForm" class="subscribe-form-container" data-aos="fade" data-aos-delay="500">
    <div class="input-field-item">
        <input id="formName" name="form-name" type="text" class="form-control input-form-control" placeholder="<?php _e('Nombre y Apellido', 'tanita'); ?>">
        <small id="errorName" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item input-phone-item">
        <span class="prefix">+52</span>
        <input id="formPhone" name="form-phone" type="text" class="form-control input-form-control" placeholder="<?php _e('Teléfono', 'tanita'); ?>">
        <small id="errorPhone" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <input id="formEmail" name="form-email" type="email" class="form-control input-form-control" placeholder="<?php _e('Correo electrónico', 'tanita'); ?>">
        <small id="errorEmail" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <select name="form-estado" id="formEstado" class="form-control input-form-control">
            <option value="" selected disabled><?php _e('Estado', 'tanita'); ?></option>
            <option value="Aguascalientes">Aguascalientes</option>
            <option value="Baja California">Baja California</option>
            <option value="Baja California Sur">Baja California Sur</option>
            <option value="Campeche">Campeche</option>
            <option value="Chiapas">Chiapas</option>
            <option value="Chihuahua">Chihuahua</option>
            <option value="CDMX">Ciudad de México</option>
            <option value="Coahuila">Coahuila</option>
            <option value="Colima">Colima</option>
            <option value="Durango">Durango</option>
            <option value="Estado de México">Estado de México</option>
            <option value="Guanajuato">Guanajuato</option>
            <option value="Guerrero">Guerrero</option>
            <option value="Hidalgo">Hidalgo</option>
            <option value="Jalisco">Jalisco</option>
            <option value="Michoacán">Michoacán</option>
            <option value="Morelos">Morelos</option>
            <option value="Nayarit">Nayarit</option>
            <option value="Nuevo León">Nuevo León</option>
            <option value="Oaxaca">Oaxaca</option>
            <option value="Puebla">Puebla</option>
            <option value="Querétaro">Querétaro</option>
            <option value="Quintana Roo">Quintana Roo</option>
            <option value="San Luis Potosí">San Luis Potosí</option>
            <option value="Sinaloa">Sinaloa</option>
            <option value="Sonora">Sonora</option>
            <option value="Tabasco">Tabasco</option>
            <option value="Tamaulipas">Tamaulipas</option>
            <option value="Tlaxcala">Tlaxcala</option>
            <option value="Veracruz">Veracruz</option>
            <option value="Yucatán">Yucatán</option>
            <option value="Zacatecas">Zacatecas</option>
        </select>
        <small id="errorEstado" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item submit-field-item">
        <button id="subscribeFormBtn" type="submit" class="btn btn-md btn-form"><?php echo get_post_meta(get_the_ID(), 'tnt_form_button_text', true); ?></button>
        <div class="loader-css d-none"></div>
    </div>
</form>